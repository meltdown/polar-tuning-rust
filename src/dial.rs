#[cfg_attr(test, derive(Debug, PartialEq))]
crate struct Dial {
    crate data: [u8; 8],
    crate enc: u8,
    crate should_transition: bool,
    crate current_position: usize,
    crate correct_position: usize,
}

impl Dial {
    crate fn new(data: [u8; 2]) -> Dial {
        let [high, low] = data; // Get the high and low bytes from the slice

        let mut dial = Dial {
            data: [
                // The bit-shifting below turns the two `u8` bytes into eight lots of three bits
                limit(high << 6),
                limit(high << 4),
                limit(high << 2),
                limit(high >> 5),
                limit(low << 6),
                limit(low << 4),
                limit(low << 2),
                limit(low >> 5),
            ],
            current_position: 7,
            correct_position: 4,
            enc: 0,
            should_transition: true,
        };

        // Set the current position
        dial.correct_position = Dial::position_from_seed(&dial.data);
        dial.data[dial.correct_position] = 0;

        dial
    }

    crate fn default() -> [Dial; 4] {
        // This is mostly used to allocate memory when constructing the dials from a seed
        return [Dial::new([0, 0]), Dial::new([0, 0]), Dial::new([0, 0]), Dial::new([0, 0])]
    }

    crate fn is_solved(&self) -> bool {
        // The dial is solved if the data at its current position matches the original data
        self.correct_data() == self.shifted_data()
    }

    crate fn move_position(&mut self, pin_a: bool, pin_b: bool) {
        // Shift the pin values onto the encoder history
        self.enc = ((self.enc << 6) >> 6 << 1) | pin_a as u8;
        self.enc = (self.enc << 1) | pin_b as u8;

        // Convert the new encoder value into a transition (or nothing)
        match self.enc {
            0b0001 | 0b0111 | 0b1000 | 0b1110 => {
                if self.should_transition {
                    self.should_transition = false;
                    self.current_position = (self.current_position + 1) % 8
                }
            },
            0b0010 | 0b0100 | 0b1011 | 0b1101 => {
                if self.should_transition {
                    self.should_transition = false;
                    self.current_position = match self.current_position {
                        0 => 7,
                        position => position - 1
                    }
                }
            },
            0b1111 => {
                self.should_transition = true;
            },
            _ => ()
        };
    }

    // This shifts the data with a default position of the dial's correct position
    crate fn correct_data(&self) -> [u8; 8] {
        self.shifted_data_with(self.correct_position)
    }

    // This shifts the data with a default position of the dial's current position
    crate fn shifted_data(&self) -> [u8; 8] {
        self.shifted_data_with(self.current_position)
    }

    /*
     * This shifts the dial data around, matching its current position.
     *
     * Effectively what that means is that if data is [1, 2, 3, 4] and the current position was `2`, the shifted data
     * would be [3, 4, 1, 2]. A shifted position of `1` would be [2, 3, 4, 1].
     */
    fn shifted_data_with(&self, position: usize) -> [u8; 8] {
        // Split the data at the position
        let (left, right) = self.data.split_at(position);

        // Initialise an array to use as the shifted data
        let mut shifted = [0u8; 8];

        // Join the split data in the other order
        for (i, x) in right.iter().chain(left).enumerate() {
            // We're doing iter chaining as I can't see how to join slices in no_std rust. This means we iterate over
            // the split data and set the shifted manually for each index
            shifted[i] = *x;
        }

        shifted
    }

    // This sets the initial position of the dial. The recursion ensures none of the dials start in a completed state.
    fn position_from_seed(seed: &[u8]) -> usize {
        match seed {
            // If we have gone through the array and still have no answer, just set a position
            [] => 2,
            _ => {
                // Get the sum of the seed bytes
                let sum : u8 = seed.iter().fold(0, |total, item| *item + total);

                // Set based on the arbitrary modulo of the sum of the seed array and the resolution
                match sum % 8 {
                    // 7 returned, so try again by dropping one item from the seed index (as 7 is the start position)
                    7 => {
                        Dial::position_from_seed(&seed[0..(seed.len() - 1)])
                    }
                    // Not zero, so return this position
                    position => position as usize
                }
            }
        }
    }
}

// This ensures the number passed in is less than six, as we have five states (four LEDs + off) and add one to ensure
// only one position is the correct answer
fn limit(n: u8) -> u8 {
    (n >> 5) % 5 + 1
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn creation() {
        assert_eq!(Dial {
            data: [3, 2, 3, 1, 2, 0, 5, 1],
            current_position: 7,
            correct_position: 5,
            enc: 0,
            should_transition: true,
        }, Dial::new([125, 231]));

        assert_eq!(Dial {
            data: [5, 1, 1, 0, 5, 2, 3, 1],
            current_position: 7,
            correct_position: 3,
            enc: 0,
            should_transition: true,
        }, Dial::new([42, 82]));

        assert_eq!(Dial {
            data: [1, 3, 3, 1, 3, 1, 0, 1],
            current_position: 7,
            correct_position: 6,
            enc: 0,
            should_transition: true,
        }, Dial::new([212, 193]));
    }

    #[test]
    fn is_solved() {
        let mut dial = Dial {
            data: [1, 2, 3, 0, 1, 2, 3, 0],
            current_position: 7,
            correct_position: 2,
            enc: 0,
            should_transition: true,
        };

        assert!(!dial.is_solved());

        dial.current_position = 2;
        assert!(dial.is_solved());
    }

    #[test]
    fn shifted_data() {
        let mut dial = Dial {
            data: [1, 2, 3, 0, 1, 2, 3, 0],
            current_position: 0,
            correct_position: 0,
            enc: 0,
            should_transition: true,
        };

        assert_eq!([1, 2, 3, 0, 1, 2, 3, 0], dial.shifted_data());

        dial.current_position = 1;
        assert_eq!([2, 3, 0, 1, 2, 3, 0, 1], dial.shifted_data());

        dial.current_position = 2;
        assert_eq!([3, 0, 1, 2, 3, 0, 1, 2], dial.shifted_data());

        assert_eq!([0, 1, 2, 3, 0, 1, 2, 3], dial.shifted_data_with(3));
        assert_eq!([1, 2, 3, 0, 1, 2, 3, 0], dial.shifted_data_with(4));
    }
}
