use crate::dial::Dial;
use core::cmp;

#[cfg_attr(test, derive(Debug, PartialEq))]
crate struct Dials {
    crate list: [Dial; 4],
}

impl Dials {
    crate fn new(seed: &str) -> Dials {
        Dials {
            list: seed.as_bytes()
                .chunks(2)
                .take(4)
                .enumerate()
                .fold(Dial::default(), |mut list, (i, seed)| {
                    list[i] = Dial::new([seed[0], seed[1]]);
                    list
                }),
        }
    }

    crate fn decode(&mut self, encoded: &mut [bool; 8]) {
        self.list[0].move_position(encoded[0], encoded[1]);
        self.list[1].move_position(encoded[2], encoded[3]);
        self.list[2].move_position(encoded[4], encoded[5]);
        self.list[3].move_position(encoded[6], encoded[7]);
    }

    // This combines all the dials so we can display them on the polar coordinate graph. Note that an array of zeros is
    // the correct answer
    crate fn flatten(&self) -> [usize; 8] {
        self.list
            .iter()
            // Get the shifted data
            .map(|dial| dial.shifted_data())
            // Zip with the actual data, so we can compare it later
            .zip(self.list.iter().map(|dial| dial.correct_data()))
            // Map over the zipped arrays
            .map(|(shifted, correct)| {
                // In each array, map over the pairs of data, comparing each shifted position with the correct one
                shifted.iter().enumerate().fold([0; 8], |mut list, (i, shift)| {
                    // XOR the bits, so we return the correct answer iff the shift is correct, otherwise the shift
                    list[i] = (*shift ^ correct[i]) % 5;
                    list
                })
            })
            // Combine the nth element from each array (as we now have an array of arrays), getting the highest value at
            // each position - if one dial is wrong then it should override the bits in the correct dials
            .fold([0; 8], |mut list, group| {
                for (i, n) in group.iter().enumerate() {
                    list[i] = cmp::max(list[i], *n as usize);
                }

                list
            })
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn creating_from_seed() {
        let comparator = Dials {
            list: [
                Dial::new([116, 101]),
                Dial::new([115, 116]),
                Dial::new([115, 101]),
                Dial::new([101, 100]),
            ]
        };

        assert_eq!(comparator, Dials::new("testseed"));
    }

    #[test]
    fn flatten() {
        assert_eq!([1, 3, 1, 2, 4, 2, 2, 3], Dials::new("testseed").flatten());
        assert_eq!([1, 2, 4, 4, 2, 1, 4, 4], Dials::new("aaaaaaaa").flatten());

        let mut dials = Dials::new("testseed");

        dials.list[0].current_position = 3;
        dials.list[1].current_position = 6;
        dials.list[2].current_position = 3;
        dials.list[3].current_position = 6;

        assert_eq!([0, 0, 0, 0, 0, 0, 0, 0], dials.flatten());
    }
}
