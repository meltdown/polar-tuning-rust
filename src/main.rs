#![deny(unsafe_code)]
#![feature(crate_visibility_modifier)]
#![cfg_attr(test, allow(unused_imports))]
#![cfg_attr(test, allow(dead_code))]
#![cfg_attr(not(test), no_std)]
#![cfg_attr(not(test), no_main)]

#[cfg(not(test))]
extern crate panic_semihosting;

crate mod dial;
crate mod dials;

use cortex_m::asm::delay;
use stm32f1xx_hal::{prelude::*, gpio};
use embedded_hal::digital::v2::OutputPin;
use embedded_hal::digital::v2::InputPin;

type Encoders = [gpio::Pxx<gpio::Input<gpio::PullUp>>; 8];
type Columns = [gpio::Pxx<gpio::Output<gpio::PushPull>>; 8];
type Rows = [gpio::Pxx<gpio::Output<gpio::PushPull>>; 4];
type Dials = [gpio::Pxx<gpio::Output<gpio::PushPull>>; 4];

#[cfg_attr(not(test), rtfm::app(device = stm32f1xx_hal::device, peripherals = true))]
const APP: () = {
    struct Resources {
        encoders: Encoders,
        encoded: [bool; 8],
        columns: Columns,
        rows: Rows,
        dials: Dials,
    }

    #[cfg(not(test))]
    #[init]
    fn init(c: init::Context) -> init::LateResources {
        // Get access to the device specific peripherals from the peripheral access crate
        let mut rcc = c.device.RCC.constrain();

        // Acquire the GPIO peripherals
        let mut gpioa = c.device.GPIOA.split(&mut rcc.apb2);
        let mut gpiob = c.device.GPIOB.split(&mut rcc.apb2);
        let mut afio = c.device.AFIO.constrain(&mut rcc.apb2);

        let (pa15, pb3, pb4) = afio.mapr.disable_jtag(gpioa.pa15, gpiob.pb3, gpiob.pb4);

        // Assign all the pins
        init::LateResources {
            encoders: [
                gpiob.pb1.into_pull_up_input(&mut gpiob.crl).downgrade(),
                gpiob.pb0.into_pull_up_input(&mut gpiob.crl).downgrade(),
                gpioa.pa7.into_pull_up_input(&mut gpioa.crl).downgrade(),
                gpioa.pa6.into_pull_up_input(&mut gpioa.crl).downgrade(),
                gpioa.pa5.into_pull_up_input(&mut gpioa.crl).downgrade(),
                gpioa.pa4.into_pull_up_input(&mut gpioa.crl).downgrade(),
                gpioa.pa3.into_pull_up_input(&mut gpioa.crl).downgrade(),
                gpioa.pa2.into_pull_up_input(&mut gpioa.crl).downgrade(),
            ],
            encoded: [false; 8],
            columns: [
                gpiob.pb10.into_push_pull_output(&mut gpiob.crh).downgrade(),
                gpiob.pb11.into_push_pull_output(&mut gpiob.crh).downgrade(),
                gpiob.pb12.into_push_pull_output(&mut gpiob.crh).downgrade(),
                gpiob.pb13.into_push_pull_output(&mut gpiob.crh).downgrade(),
                gpiob.pb14.into_push_pull_output(&mut gpiob.crh).downgrade(),
                gpiob.pb15.into_push_pull_output(&mut gpiob.crh).downgrade(),
                gpioa.pa8.into_push_pull_output(&mut gpioa.crh).downgrade(),
                gpioa.pa9.into_push_pull_output(&mut gpioa.crh).downgrade(),
            ],
            rows: [
                gpioa.pa12.into_push_pull_output(&mut gpioa.crh).downgrade(),
                pa15.into_push_pull_output(&mut gpioa.crh).downgrade(),
                pb3.into_push_pull_output(&mut gpiob.crl).downgrade(),
                pb4.into_push_pull_output(&mut gpiob.crl).downgrade(),
            ],
            dials: [
                gpiob.pb6.into_push_pull_output(&mut gpiob.crl).downgrade(),
                gpiob.pb7.into_push_pull_output(&mut gpiob.crl).downgrade(),
                gpiob.pb8.into_push_pull_output(&mut gpiob.crh).downgrade(),
                gpiob.pb9.into_push_pull_output(&mut gpiob.crh).downgrade(),
            ],
        }
    }

    #[cfg(not(test))]
    #[idle(resources = [encoders, encoded, columns, rows, dials])]
    fn idle(c: idle::Context) -> ! {
        let mut dials = dials::Dials::new("Ϲ󃜦ݯսӦwȚ");

        // Initialise columns high
        for column in c.resources.columns.iter_mut() {
            column.set_high().unwrap();
        }

        // Initialise rows and dials low
        for pin in c.resources.rows.iter_mut().chain(c.resources.dials.iter_mut()) {
            pin.set_low().unwrap();
        }

        loop {
            // Read the encoders and update the dial state
            for (index, encoded) in c.resources.encoders.iter().enumerate() {
                c.resources.encoded[index] = encoded.is_high().unwrap();
            }

            dials.decode(c.resources.encoded);

            // Cycle through ALL THE THINGS
            for (index, row) in c.resources.rows.iter_mut().enumerate() {
                row.set_high().unwrap();
                toggle_columns(c.resources.columns, index, dials.flatten());
                row.set_low().unwrap();
            }

            toggle_dials(c.resources.dials, c.resources.columns, &dials);
        }
    }
};

fn toggle_columns(columns: &mut Columns, row: usize, dials: [usize; 8]) {
    for (dial, column) in dials.iter().zip(columns.iter_mut()) {
        if *dial <= row {
            column.set_low().unwrap();
        }
    }

    delay(1_000);

    for column in columns {
        column.set_high().unwrap();
    }
}

fn toggle_dials(leds: &mut Dials, columns: &mut Columns, dials: &dials::Dials) {
    for (index, led) in leds.iter_mut().enumerate() {
        led.set_high().unwrap();
        toggle_dial(columns, dials.list[index].current_position);
        led.set_low().unwrap();
    }
}

fn toggle_dial(columns: &mut Columns, position: usize) {
    columns[position].set_low().unwrap();
    delay(1_000);
    columns[position].set_high().unwrap();
}
